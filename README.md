[//]: # "#to make links in gitlab: example with racon https://github.com/isovic/racon"
[//]: # "tricks in markdown: https://openclassrooms.com/fr/courses/1304236-redigez-en-markdown"

| usage | R dependencies | Python dependencies |
| --- | --- | --- |
| [![R Version](https://img.shields.io/badge/code-R-blue?style=plastic)](https://cran.r-project.org/mirrors.html) | [![Dependencies: R Version](https://img.shields.io/badge/R-v3.5.3-blue?style=plastic)](https://cran.r-project.org/mirrors.html) | [![Dependencies: Python Version](https://img.shields.io/badge/python-v3.6-red?style=plastic)](https://repo.continuum.io/archive/) |
| [![License: GPL-3.0](https://img.shields.io/badge/licence-GPL%20(%3E%3D3)-green?style=plastic)](https://www.gnu.org/licenses) | [![Dependencies: R Package](https://img.shields.io/badge/package-reticulate%20v1.12-blue?style=plastic)](https://github.com/rstudio/reticulate) | [![Dependencies: Python Package](https://img.shields.io/badge/package-serpentine%20v0.1.2-red?style=plastic)](https://github.com/koszullab/serpentine) |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-lubridate%20v1.7.4-blue?style=plastic)](https://github.com/tidyverse/lubridate) |  |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-reshape2%20v1.4.3-blue?style=plastic)](https://github.com/hadley/reshape) |  |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-ggplot2%20v3.1.0-blue?style=plastic)](https://github.com/tidyverse/ggplot2) |  |
|  | [![Dependencies: R Package](https://img.shields.io/badge/package-Cairo%20v1.5.9-blue?style=plastic)](http://www.rforge.net/Cairo/) |  |
|  | [![Dependencies: R Package](https://img.shields.io/badge/toolbox-cute%20little%20R%20functions%20v6.0.0-blue?style=plastic)](https://gitlab.pasteur.fr/gmillot/cute_little_R_functions) |  |


## TABLE OF CONTENTS

   - [AIM](#aim)
   - [SLITHERINE CONTENT](#slitherine-content)
   - [HOW TO RUN SLITHERINE](#how-to-run-slitherine)
   - [SLITHERINE OUTPUT](#slitherine-output)
   - [VERSIONS](#versions)
   - [LICENCE](#licence)
   - [CITATION](#citation)
   - [CREDITS](#credits)
   - [WHAT'S NEW IN](#what's-new-in)


## AIM

Slitherine finds significant coverage differences when comparing two contact matrices using Serpentine


<div>
<p align="center"> 
  <img width="250" height="250" src="other/sliterine_logo.gif">
</p>
</div>





## SLITHERINE CONTENT

**slitherine.R** file that can be executed using a CLI (command line interface) or sourced in R or RStudio

**slitherine.config** parameter settings for the slitherin.R file

**dataset** folder containing the datasets used in the publication

| File Name | Description |
| --- | --- |
|c1_30min_5p.filt.2500.rebin_1162-1388.txt | replicate 1 (Fig2 & 4A)|
|c2_30min_5p.filt.2500.rebin_1162-1388.txt | replicate 2 (Fig2 & 4A)|
|AT_Dndj1_D_T0.filt.2500.rebin_1162-1388.txt | yeast meiosis condition 1 (Fig3 & 4B)|
|AT200_Aurel_T6.filt.2500.rebin_1162-1388.txt | yeast meiosis condition 2 (Fig3 & 4B)|
|AT_Dndj1_D_T0.filt.2500.rebin_1162-1388_chr5_subsampled_0.3.txt | down sampling 30% (Fig5)|
|AT200_Aurel_T6.filt.2500.rebin_1162-1388_chr5_subsampled_0.3.txt | down sampling 30% (Fig5)|
|AT_Dndj1_D_T0.filt.2500.rebin_1162-1388_chr5_subsampled_0.6.txt | down sampling 60% (Fig5)|
|AT200_Aurel_T6.filt.2500.rebin_1162-1388_chr5_subsampled_0.6.txt | down sampling 60% (Fig5)|
|AT_Dndj1_D_T0.filt.2500.rebin_1162-1388_chr5_subsampled_0.9.txt | down sampling 90% (Fig5)|
|AT200_Aurel_T6.filt.2500.rebin_1162-1388_chr5_subsampled_0.9.txt | down sampling 90% (Fig5)|
|mouse_control_chr2_region_27700_29160.txt | mouse interphase (Fig6)|
|mouse_pachytene_chr2_region_27700_29160.txt | mouse meoisis pachytene stage (Fig6)|
|mouse_zygotene_chr2_region_27700_29160.txt | mouse meoisis zygotene stage (Fig6)|


**example_of_result** folder containing an example of result obtained with slitherine, using the slitherine_R file as well as the AT_Dndj1_D_T0.filt.2500.rebin_1162-1388.txt and AT200_Aurel_T6.filt.2500.rebin_1162-1388.txt matrices (Figure 3 of the publication)

**other** folder containing gitlab web interface files as well as other non essential documents. Note that the slitherine_config.doc file can facilitate the reading of the parameter setting description


## HOW TO RUN SLITHERINE

### Using a R GUI (graphic user interface, i.e., R or RStudio windows)

1) Open the slitherine.config file and set the parameters. The file must be present in the same directory as slitherine.R

2) Open R or RStudio

3) Source the slitherine.R file, for instance using the following instruction:

	`  source("C:/Users/Gael/Desktop/slitherine.R")  `


### Using a R CLI (command line interface)

1) Open the slitherine.config file and set the parameters. The file must be present in the same directory as slitherine.R

2) Open a shell windows

3) run slitherine.R, for instance using the following instruction:

	`  Rscript slitherine.R slitherine.config  `
For cygwin, use something like:

`  /cygdrive/c/Program\ Files/R/R-3.5.3/bin/Rscript slitherine.R slitherine.config  `


## SLITHERINE OUTPUT

| Mandatory Files | Description |
| --- | --- |
| **all_objects.RData** | R file containing all the objects created during the run. Open it to inspects the objects |
| **cor_parall_log.txt** | log file showing the parallelization progress during the correlation step |
| **correlation_plots_xxxxxxxxxx.pdf** | correlation plots |
| **cv1_cv2_rho1_rho2_backup.RData** | R backup file containing cv1, cv2, rho1 and rho2. This file that can be used by the cv.rho.obtained and path.cv.rho parameters |
|    	- **cv1** | selected coefficient of variation from observed matrix 1 (see plots_xxxxxxxxxx.pdf) used to generate the theoretical diagonals of theoretical matrix 1 |
|    	- **cv2** | idem *cv1* but for matrix 2 |
|    	- **rho1** | Spearman coefficient of correlation between diagonal values from observed matrices 1 and 2 (see plots_xxxxxxxxxx.pdf) used to generate the theoretical diagonals of theoretical matrix 1 and 2 |
|    	- **rho2** | strictly identical to *rho1* |
| **inf_mask_pre_serp.txt** | matrix of the mask pre serpentine indicating the observed Mat2 cells << observed Mat1 cells (Mat2 / Mat1 << 1). See plots_xxxxxxxxxx.pdf |
| **mat1.theo.txt** | theoretical matrix 1 generated from the matrix 1 imported (saved just before the serpentine binning step)|
| **mat2.theo.txt** | idem *mat1.theo.txt* but for matrix 2 |
| **mask_pre_serp.txt** | matrix of the mask pre serpentine indicating the observed Mat2 cells >> observed Mat1 cells (Mat2 / Mat1 >> 1) and observed Mat2 cells << observed Mat1 cells (Mat2 / Mat1 << 1). See plots_xxxxxxxxxx.pdf |
| **plots_xxxxxxxxxx.pdf** | all the plots, except the correlation plots |
| **segmentation_pre_serp.RData** | R file containing all the objects resulting from the analysis of significance pre serpentine. Open it to inspects the objects |
|    - **obs** | cell-cell comparison between observed matrix 1 and 2: MEAN, RATIO (mat2 / mat1), MATRICES (Obs or Theo, here Obs), coord_1D (One dimension coordinate of the 2 compared cells in the obs matrices, from 1 to nb rows x nb colums). Potentially log transformed depending on the `transfo` parameter setting |
|    - **theo** | idem *obs* but for theoretical matrices |
|    - **signif.obs.dot.pre** | significant cells after segmentation and after `ratio.limit.sig` parameter setting (extraction from obs) |
|    - **inf.signif.obs.dot.pre** | idem *signif.obs.dot.pre* but inly the cells for which matrix 2 cells << matrix 1 cells |
|    - **sup.signif.obs.dot.pre** | idem *signif.obs.dot.pre* but inly the cells for which matrix 2 cells >> matrix 1 cells |
|    - **signif.theo.dot.pre** | idem *signif.obs.dot.pre* but for theoretical matrices |
|    - **segment.pre.serp** | list containing all the results from the fun_segmentation() function coming from the cute little R function toolbox. See https://gitlab.pasteur.fr/gmillot/cute_little_R_functions for the details |
| **slitherine_xxxxxxxxxx_report.txt** | report file |
| **sup_mask_pre_serp.txt** | matrix of the mask pre serpentine indicating the observed Mat2 cells >> observed Mat1 cells (Mat2 / Mat1 >> 1). See plots_xxxxxxxxxx.pdf |

| Optional Files (hiccomp = TRUE) | Description |
| --- | --- |
| **hicc_pvalue_mask_pre_serp.txt** | matrix of the mask pre serpentine obtained with the HiC Compare package and corresponding to the significant cells on the differential heatmap. See plots_xxxxxxxxxx.pdf |
| **hicc_padj_mask_pre_serp.txt** | idem *hicc_pvalue_mask_pre_serp.txt* but using adjusted p values (benjamini hochberg) |
| **hicc_pvalue_mask_post_serp.txt** | idem *hicc_pvalue_mask_pre_serp.txt* but after serpentine binning (serp.binning = TRUE) |
| **hicc_padj_mask_post_serp.txt** | idem *hicc_padj_mask_pre_serp.txt* but after serpentine binning (serp.binning = TRUE) |

| Optional Files (serp.binning = TRUE) | Description |
| --- | --- |
| **inf_mask_post_serp.txt** | idem *inf_mask_pre_serp.txt* but post serpentine |
| **mask_post_serp.txt** | idem *mask_pre_serp.txt* but post serpentine |
| **mat1.obs.serp.txt** | first observed matrix (corresponding to file.name1 user parameter) after serpentine binning |
| **mat1.theo.serp.txt** | first theoretical matrix (associated to mat1.obs.serp.txt) after serpentine binning |
| **mat2.obs.serp.txt** | second observed matrix (corresponding to file.name2 user parameter) after serpentine binning |
| **mat2.theo.serp.txt** | second theoretical matrix (associated to mat2.obs.serp.txt) after serpentine binning |
| **obs_serp_parall_log.txt** | log file of the serpentine parallelization step |
| **segmentation_post_serp.RData** | idem *segmentation_pre_serp.RData* but post serpentine |
| **sup_mask_post_serp.txt** | idem *sup_mask_pre_serp.txt* but post serpentine |
| **theo_serp_parall_log.txt** | log file reporting all the information of the slitherine run |


Additional .RData files can be present, depending on the setting of parameter `keep`. Such files are described in the *slitherine_xxxxxxxxxx_report.txt* file


## VERSIONS

The different releases are tagged [here](https://gitlab.pasteur.fr/gmillot/slitherine/-/tags)


## LICENCE

This package of scripts can be redistributed and/or modified under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Distributed in the hope that it will be useful, but without any warranty; without even the implied warranty of merchandability or fitness for a particular purpose.
See the GNU General Public License for more details at https://www.gnu.org/licenses.


## CITATION

Not yet published


## CREDITS

[Gael A. Millot](https://gitlab.pasteur.fr/gmillot), Hub-CBD, Institut Pasteur, USR 3756 IP CNRS, Paris, France

[Vittore F. Scolari](https://github.com/scovit) (a.k.a. Cluster Buster), Regulation Spatiale des Chromosomes, Institut Pasteur, Paris, France

[Lyam Baudry](https://github.com/baudrly) (a.k.a. Lyamvovitch), Regulation Spatiale des Chromosomes, Institut Pasteur, Paris, France



## WHAT'S NEW IN


### v1.0.0

Everything


