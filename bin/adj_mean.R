#! /usr/bin/Rscript



adj.mean.fun <- function(x_3fun, text_3fun){
    # AIM
    # normalization of theoretical matrices
    
    # WARNING
    # not appropriate for observed matrix normalization
    # never use default value for arguments of this function (to be sure)
    
    # EXPLANATION
    # division of all the cells by the cell mean: c[ij] / mean(c[ij]) 
    # with mean(c[ij]) = sum(c[ij]) / n^2 with n the dimension of the squre matrix
    # cant do this for theo matrices <- not the same structure 
    # matrix is dimension k * n (k rows and n column, each column representing a diagonal of obs matrix, 
    # rom main (left) to corner (right) diagonals). Thus means of each theo column is about equal to mean of diagonals of obs matrix
    # In obs matrix, the corner diagonal is represented by 2 (2 * 1) cells when computing the mean (because square matrix), 
    # while represented by nrow = k cells in the theo matrix. Thus, if we want to apply the same mean formula as in obs matrix, 
    # cells of the last column of theo must be divided by k and multiplied by 2: (c[in] / k) * 2
    # Then, in obs matrix, the previous n-1 corner diagonal is represented by 4 (2 * 2) cells when computing the mean (because square matrix), 
    # while represented by nrow = k cells in the theo matrix. Thus, if we want to apply the same mean formula as in obs matrix, 
    # cells of the previous last column of theo must be divided by k and multiplied by 4: (c[in-1] / k) * 4
    # Etc., for the first column of theo matrix: (c[i1] / k) * n*2
    # Finally, compute a theo matrix normalized according to weighted column mean 
    # (each column represent the obs diagonals from main (left) to corner (right) diagonals). 
    # The weighted column mean is according to the number of cells per diagonal, 
    # such as the main diagonal has the max weight and the corner the min. Important for theo matrices because the corner diagonal has n cells, 
    # while in the obs matrices, it has 1 cell. Thus, the matrix computed is adjusted by the number of cells in diag of obs matrices

    # ARGUMENTS
    # x_3fun: a matrix
    # text_3fun: character string of optional text for error messages
    
    # REQUIRED PACKAGES
    # none
    # REQUIRED FUNCTIONS FROM CUTE_LITTLE_R_FUNCTION
    # fun_check()
    # RETURN
    # a numeric mean value
    # EXAMPLES
    # DEBUGGING
    # function name
    function.name <- paste0(as.list(match.call(expand.dots=FALSE))[[1]], "()")
    function.name <- paste0(function.name, " INTERNAL FUNCTION OF SLITHERINE")
    # end function name
    # required functions and argument checking
    
    #internal.function.check.fun(
    #    expect.arg.values_fun = list(
    #        L1 = c( # add here all the values found in the code for expect.arg.names[1]
    #            "theo.mean.mat", 
    #            "get(loop.mat.names[i0])", 
    #            "get(loop.mat.names[i0])", 
    #            "mat1.ini",
    #            "mat2.ini",
    #            "mat1.mix", 
    #            "mat2.mix", 
    #            "mat.diff.theo.serp", 
    #            "mat1.theo.serp", 
    #            "mat2.theo.serp"
    #        ), 
    #        L2 = c( # add here all the values found in the code for expect.arg.names[2]
    #            "", 
    #            "JUST AFTER IMPORT THEO HEATMAP NORMALIZATION ACCORDING TO THE WEIGHTED DIAGONAL MEAN", 
    #            "JUST AFTER CREATION OF THEO, HEATMAP NORMALIZATION ACCORDING TO THE WEIGHTED DIAGONAL MEAN", 
    #            "JUST AFTER CREATION, THEO HEATMAP NORMALIZATION ACCORDING TO THE WEIGHTED DIAGONAL MEAN", 
    #            "AFTER SERPENTINE THEO HEATMAP NORMALIZATION ACCORDING TO THE WEIGHTED DIAGONAL MEAN"
    #        )
    #    ),
    #    req.functions_fun = "fun_check",
    #    function.name_fun = function.name
    #)
    # end required functions and argument checking
    # argument content checking
    #arg.check <- NULL # for function debbuging
    #checked.arg.names <- NULL # for function debbuging: used by r_debugging_tools
    #ee <- expression(arg.check <- c(arg.check, tempo$problem) , checked.arg.names <- 
    #                     c(checked.arg.names, tempo$param.name))
    #tempo <- fun_check(data = x_3fun, class = "matrix", mode = "numeric", 
    #                   na.contain = TRUE, fun.name = function.name) ; eval(ee)
    #tempo <- fun_check(data = text_3fun, class = "vector", mode = "character", 
    #                   length = 1, fun.name = function.name) ; eval(ee)
    #if(any(arg.check) == TRUE){
    #    stop() # nothing else because print = TRUE by default in fun_check()
    #}
    # end argument content checking
    # main code
    tempo.matrix <- sweep(x_3fun, 2,  2 * (ncol(x_3fun):1) / nrow(x_3fun), FUN = "*") # weighted according to the number of cells in each diag of obs matrix, as explained above
    mean.output <- mean(tempo.matrix[is.finite(tempo.matrix)], na.rm = TRUE)
    if(all(is.na(mean.output)) | all( ! is.finite(mean.output))){
        tempo.cat <- paste0("\n\n============\n\n", ifelse(text_3fun == "", 
                                                           "", paste0(text, "\n")), "ERROR IN ", function.name, ": THE ADJUSTED MEAN IS NA, NaN OR Inf: ", 
                            paste(mean.output, collapse = " "), "\n\n============\n\n")
        stop(tempo.cat)
    }else{
        return(mean.output)
    }
}
