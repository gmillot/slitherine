nextflow.enable.dsl=2
/*
#############################################################
##                                                         ##
##     script.nf                                           ##
##     Slitherine                                          ##
##                                                         ##
##     Mia Legras                                          ##
##     Gael A. Millot                                      ##
##     Bioinformatics and Biostatistics Hub                ##
##     Institut Pasteur Paris                              ##
##                                                         ##
#############################################################
*/



//////// Processes


process workflowParam { // create a file with the workflow parameters in out_path
    label 'bash'
    publishDir "${out_path}/reports", mode: 'copy', overwrite: false
    cache 'false'

    input:
    val modules

    output:
    path "Run_info.txt"

    script:
    """
    echo "Project (empty means no .git folder where the main.nf file is present): " \$(git -C ${projectDir} remote -v | head -n 1) > Run_info.txt # works only if the main script run is located in a directory that has a .git folder, i.e., that is connected to a distant repo
    echo "Git info (empty means no .git folder where the main.nf file is present): " \$(git -C ${projectDir} describe --abbrev=10 --dirty --always --tags) >> Run_info.txt # idem. Provide the small commit number of the script and nextflow.config used in the execution
    echo "Cmd line: ${workflow.commandLine}" >> Run_info.txt
    echo "execution mode": ${system_exec} >> Run_info.txt
    modules=$modules # this is just to deal with variable interpretation during the creation of the .command.sh file by nextflow. See also \$modules below
    if [[ ! -z \$modules ]] ; then
        echo "loaded modules (according to specification by the user thanks to the --modules argument of main.nf): ${modules}" >> Run_info.txt
    fi
    echo "Manifest's pipeline version: ${workflow.manifest.version}" >> Run_info.txt
    echo "result path: ${out_path}" >> Run_info.txt
    echo "nextflow version: ${nextflow.version}" >> Run_info.txt
    echo -e "\\n\\nIMPLICIT VARIABLES:\\n\\nlaunchDir (directory where the workflow is run): ${launchDir}\\nprojectDir (directory where the main.nf script is located): ${projectDir}\\nworkDir (directory where tasks temporary files are created): ${workDir}" >> Run_info.txt
    echo -e "\\n\\nUSER VARIABLES:\\n\\nout_path: ${out_path}\\in_path: ${in_path}" >> Run_info.txt
    """
}
//${projectDir} nextflow variable
//${workflow.commandLine} nextflow variable
//${workflow.manifest.version} nextflow variable
//Note that variables like ${out_path} are interpreted in the script block


process PRE_SERPENTINE {
    label 'R'
    publishDir "${out_path}", mode: 'copy', pattern: "{*.txt, *.RData, *.pdf}", overwrite: false 
    // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
    cache 'true'

    input:
    path mat1 
    path mat2
    val theo_path_in // can be NULL
    path mat1_theo // NULL if does not exists
    path mat2_theo // NULL if does not exists
    path cute_file
    path lib_path
    path path_cv_rho // can be "NULL"
    path path_theo1_theo2 // can be "NULL"
    val project_name
    val empty_cell_string
    val thread_nb 
    val hiccomp 
    val binning 
    val n_row 
    val win_size 
    val single_corr 
    val abs_corr_limit
    val print_count 
    val keep 
    val ratio_limit_sig 
    val error 
    val range_split 
    val step_factor 
    val ratio_normalization 
    val activate_pdf 
    val optional_text 
    val width_wind 
    val height_wind 
    val dot_size 
    val line_size 
    val heatmap_text_size 
    val text_size 
    val title_text_size 
    val raster 
    val transfo 
    val warn_secu
    val serp_symmet_input
    val adj_mean
    val hiccompare_graph
    val mask_plot

    output:
    path "mat1.obs.txt", emit: matrix_norm_ch1
    path 'mat2.obs.txt', emit: matrix_norm_ch2
    path 'mat1.theo.txt', emit: matrix_theo_ch1
    path 'mat2.theo.txt', emit: matrix_theo_ch2
    path '*.RData'
    path '*.pdf'

    script:
    """
    #!/bin/bash -ue
    slitherine_part1.R \
"${mat1}" \
"${mat2}" \
"${theo_path_in}" \
"${mat1_theo}" \
"${mat2_theo}" \
"${cute_file}" \
"${lib_path}" \
"${path_cv_rho}" \
"${path_theo1_theo2}" \
"${project_name}" \
"${empty_cell_string}" \
"${thread_nb}" \
"${hiccomp}" \
"${binning}" \
"${n_row}" \
"${win_size}" \
"${single_corr}" \
"${abs_corr_limit}" \
"${print_count}" \
"${keep}" \
"${ratio_limit_sig}" \
"${error}" \
"${range_split}" \
"${step_factor}" \
"${ratio_normalization}" \
"${activate_pdf}" \
"${optional_text}" \
"${width_wind}" \
"${height_wind}" \
"${dot_size}" \
"${line_size}" \
"${heatmap_text_size}" \
"${text_size}" \
"${title_text_size}" \
"${raster}" \
"${transfo}" \
"${warn_secu}" \
"${serp_symmet_input}" \
"${adj_mean}" \
"${hiccompare_graph}" \
"${mask_plot}"
        """
// the difference between the 2 is just: ${mat1_theo}" "${mat2_theo}" "${theo_path_in}
} 

if (serp_binning == true){
    process SERPENTINE {
        label 'python'
        publishDir "${out_path}", mode: 'copy', pattern: "{*.csv}", overwrite: false 
        // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
        cache 'true'

        input:
        path matrix1 from matrix_norm_ch1
        path matrix2 from matrix_norm_ch2
        path matrix1_theo from matrix_theo_ch1
        path matrix2_theo from matrix_theo_ch2
        val thr_tot
        val thr_min

        output:
        path 'mat1_bin.csv' into matrixbin_ch1
        path 'mat2_bin.csv' into matrixbin_ch2
        path 'mat1_theo_bin.csv' into matrixbin_theo_ch1
        path 'mat2_theo_bin.csv' into matrixbin_theo_ch2    

        script:
        """
        use_serp.py "${matrix1}" "${matrix2}" "${matrix1_theo}" "${matrix2_theo}" "${thr_tot}" "${thr_min}"
        """
    }

    process POST_SERPENTINE {
        label 'R'
        publishDir "${out_path}", mode: 'copy', pattern: "{*.txt, *.RData, *.pdf}", overwrite: false 
        // https://docs.oracle.com/javase/tutorial/essential/io/fileOps.html#glob
        cache 'true'

        input:
        val project_name
        stdin path_function1
        path mat1obs from matrixbin_ch1
        path mat2obs from matrixbin_ch2
        path mat1theo from matrixbin_theo_ch1
        path mat2theo from matrixbin_theo_ch2
        val hiccomp 
        val binning 
        val keep 
        val ratio_limit_sig 
        val error 
        val range_split 
        val step_factor 
        val ratio_normalization 
        val activate_pdf 
        val optional_text 
        val width_wind 
        val height_wind 
        val dot_size 
        val line_size 
        val heatmap_text_size 
        val text_size 
        val title_text_size 
        val raster 
        val transfo 
        val serp_symmet_input
        val adj_mean
        val hiccompare_graph
        val intern_funct_check
        val mask_plot

        output:
        path "*.txt"
        path "*.RData"
        path "*.pdf"

        script:
            """
            slith_part2.R "${project_name}" "./" "./" "${path_function1}" \
                "${mat1obs}" "${mat2obs}" "${mat1theo}" "${mat2theo}" "${hiccomp}" \
                "${binning}" "${keep}" "${ratio_limit_sig}" "${error}" "${range_split}" \
                "${step_factor}" "${ratio_normalization}" "${activate_pdf}" "${optional_text}" \
                "${width_wind}" "${height_wind}" "${dot_size}" "${line_size}" "${heatmap_text_size}" \
                "${text_size}" "${title_text_size}" "${raster}" "${transfo}" "${warn_secu}" \
                "${serp_symmet_input}" "${adj_mean}" "${hiccompare_graph}" "${intern_funct_check}" "${mask_plot}"
            """
    }
}


process backup {
    label 'bash'
    publishDir "${out_path}/reports", mode: 'copy', overwrite: false // since I am in mode copy, all the output files will be copied into the publishDir. See \\wsl$\Ubuntu-20.04\home\gael\work\aa\a0e9a739acae026fb205bc3fc21f9b
    cache 'false'

    input:
    path config_file
    path log_file

    output:
    path "${config_file}" // warning message if we use path config_file
    path "${log_file}" // warning message if we use path log_file
    path "Log_info.txt"

    script:
    """
    #!/bin/bash -ue
    echo -e "full .nextflow.log is in: ${launchDir}\nThe one in the result folder is not complete (miss the end)" > Log_info.txt
    """
}


//////// End Processes

//////// Workflow





workflow {

    //////// Options of nextflow run

    print("\n\nINITIATION TIME: ${workflow.start}")

    //////// end Options of nextflow run


    //////// Options of nextflow run

    // --modules (it is just for the process workflowParam)
    params.modules = "" // if --module is used, this default value will be overridden
    // end --modules (it is just for the process workflowParam)

    //////// end Options of nextflow run


    //////// Variables

    modules = params.modules // remove the dot -> can be used in bash scripts
    config_file = workflow.configFiles[0] // better to use this than config_file = file("${projectDir}/ig_clustering.config") because the latter is not good if -c option of nextflow run is used
    log_file = file("${launchDir}/.nextflow.log")

    //////// end Variables


    //////// Checks




    // below : those variable are already used in the config file. Thus, to late to check them. And not possible to check inside the config file
    // out_ini
    print("\n\nRESULT DIRECTORY: ${out_path}")
    print("\n\nWARNING: PARAMETERS ALREADY INTERPRETED IN THE .config FILE:")
    print("    system_exec: ${system_exec}")
    print("    out_path: ${out_path_ini}")
    if("${system_exec}" != "local"){
        print("    queue: ${queue}")
        print("    qos: ${qos}")
        print("    add_options: ${add_options}")
    }
    print("\n\n")

    //////// end Checks


    //////// Variable modification


    //////// end Variable modification


    //////// Channels


    //////// end Channels


    //////// files import

    mat1 = file("${in_path}/${matrix_file1}")
    mat2 = file("${in_path}/${matrix_file2}")
    mat1_theo = file("${in_path}/${theo_file_name1}") // NULL file if does not exists
    mat2_theo = file("${in_path}/${theo_file_name2}") // NULL file if does not exists
    cute_file = file("${path_function1}") // in variable because a single file
    adj_mean = file("${adj_mean}") // in variable because a single file
    hiccompare_graph = file("${hiccompare_graph}") // in variable because a single file
    intern_funct_check = file("${intern_funct_check}") // in variable because a single file
    mask_plot = file("${mask_plot}") // in variable because a single file
    lib_path = file("${lib_path}") // in variable because a single folder
    path_cv_rho = file("${path_cv_rho}") // in variable because a single file
    path_theo1_theo2 = file("${path_theo1_theo2}") // in variable because a single folder


    //////// end files import


    //////// Main

    workflowParam(
        modules
    )



    PRE_SERPENTINE(
        mat1,
        mat2,
        theo_path_in, 
        mat1_theo,
        mat2_theo,
        cute_file,
        lib_path,
        path_cv_rho,
        path_theo1_theo2,
        project_name,
        empty_cell_string,
        thread_nb,
        hiccomp,
        binning,
        n_row,
        win_size,
        single_corr,
        abs_corr_limit,
        print_count,
        keep,
        ratio_limit_sig,
        error,
        range_split,
        step_factor,
        ratio_normalization,
        activate_pdf,
        optional_text,
        width_wind,
        height_wind,
        dot_size,
        line_size,
        heatmap_text_size,
        text_size,
        title_text_size,
        raster,
        transfo,
        warn_secu,
        serp_symmet_input,
        adj_mean,
        hiccompare_graph,
        mask_plot
    )

}





